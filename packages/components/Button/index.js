import { h } from "vue";
import { useRouter } from "vue-router";

import AuthButton from "./button.vue";
const insertBtn = (type) => ({
  // functional: true,
  name: `YjAuth${type[0].toUpperCase()}${type.slice(1)}`,
  props: { type: String, visible: Boolean, label: String },
  render(par) {
    const router = useRouter();
    const meta = router ? router.currentRoute.value.meta : {};
    const btnPer = meta["btnPermissions"] || [];
    return h(AuthButton, {
      attrs: { ...par.$data.attrs },
      ...par.$props,
      visible: btnPer.includes(type) || false,
    });
  },
});
// 增
export const YjAuthAdd = insertBtn("add", "添加");
// 编辑
export const YjAuthEdit = insertBtn("edit", "编辑");
export const YjAuthDel = insertBtn("del", "删除");

export const YjAuth = AuthButton;
