// 防抖
const _debounce = (fn, delay = 1000) => {
  let timer;
  return function () {
    let t = this;
    let args = arguments;
    if (timer) {
      clearTimeout(timer);
    }

    timer = setTimeout(() => {
      timer = null;
      fn.apply(t, args);
    }, delay);
  };
};

// 节流
const _throttle = (fn, interval = 1000) => {
  let timer;
  let last;
  return function () {
    let t = this;
    let args = arguments;
    let now = +new Date();
    if (last && now - last < interval) {
      clearTimeout(timer);
      timer = setTimeout(() => {
        last = now;
        fn.apply(t, args);
      }, interval);
    } else {
      last = now;
      fn.apply(t, args);
    }
  };
};

// 计算精度损失问题
const _countAdd = (...args) => {
  const arr = args;
  //   分别找出小数点的位数
  const deciArr = _decimalNum(arr);
  //   计算小数点位数最多的值
  const max = Math.max(...deciArr) == -Infinity ? 1 : Math.max(...deciArr);
  //   数组中的值换算成整数
  const newArr = [];
  const powNum = Number(Math.pow(10, max));
  arr.forEach((item) => {
    newArr.push(Number(powNum * item));
  });
  const count = newArr.map(Number).reduce((a, b) => a + b);
  return count / powNum;
};

const _decimalNum = (args) => {
  const arr = [];
  args.forEach((item) => {
    if (item.toString().indexOf(".") > 0) {
      const i = item.toString().split(".")[1].length;
      arr.push(i);
    }
  });
  return arr;
};

export default { _debounce, _throttle, _countAdd, _decimalNum };
