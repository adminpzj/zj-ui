import "element-plus/dist/index.css";

import Search from "./components/Search/index.vue";
import YHeader from "./components/Header/index.vue";
import {
  YjAuth,
  YjAuthAdd,
  YjAuthEdit,
  YjAuthDel,
} from "./components/Button/index";

// 公共方法
import utils from "./utils/index";
const comps = [Search, YjAuth, YjAuthAdd, YjAuthEdit, YjAuthDel, YHeader];
const install = function (Vue) {
  comps.forEach((element) => {
    if (element.name) Vue.component(element.name, element);
  });
};

export default { install };
// 导出组件install
export { Search as YSearch, utils };
